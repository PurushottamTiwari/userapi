package com.example.userapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.example.aspects","com.example.userapi","com.example.exceptionhandling"})


public class UserapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserapiApplication.class, args);
    }

}
