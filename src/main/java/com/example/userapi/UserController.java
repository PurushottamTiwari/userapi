package com.example.userapi;

import com.example.aspects.TokenRequired;
import com.example.exceptionhandling.CustomExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.List;

@RestController
public class UserController extends CustomExceptionHandler {

    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping(path = "/add",method = RequestMethod.POST)
    @TokenRequired
    public ResponseEntity<UserResponse> addUser(@RequestBody User user) throws Exception{
        logger.debug("...........Inside AddUser...............");
        UserResponse userResponse = new UserResponse();
        ResponseEntity responseEntity = null;
        user.setLastUpdate(new Date());
        userResponse.setOperationName("Add User");
        userService.addUser(user);
        userResponse.setOperationResult("SUCCESS");
        logger.debug("UserResponse :"+userResponse);
        responseEntity =  new ResponseEntity(userResponse,HttpStatus.CREATED);
        return responseEntity;
    }

    @RequestMapping(path = "/update",method = RequestMethod.POST)
    @TokenRequired
    public ResponseEntity updateUser(@RequestBody User user) throws Exception{
        logger.debug(".............Inside UpdateUser.............");
        UserResponse userResponse = new UserResponse();
        ResponseEntity responseEntity = null;
        user.setLastUpdate(new Date());
        userResponse.setOperationName("Update User");
        userService.updateUser(user);
        userResponse.setOperationResult("SUCCESS");
        logger.debug("UserResponse :"+userResponse);
        responseEntity =  new ResponseEntity(userResponse,HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(path = "/fetch/{userid}",method = RequestMethod.GET)
    @TokenRequired
    public ResponseEntity getUser(@PathVariable("userid") int userId, WebRequest webRequest) throws Exception{
        logger.debug("..............Inside GetUser..................");
        UserResponse userResponse = new UserResponse();
        ResponseEntity responseEntity = null;
        User usr =  userService.getUser(userId);
        logger.debug("NotModified Check :"+webRequest.checkNotModified(usr.getLastUpdate().getTime()));
        if(webRequest.checkNotModified(usr.getLastUpdate().getTime())){
            responseEntity =  new ResponseEntity(HttpStatus.NOT_MODIFIED);
        }else{
            responseEntity =  new ResponseEntity(usr,HttpStatus.OK);
        }
        return responseEntity;
    }

    @RequestMapping(path = "/user/list",method = RequestMethod.GET)
    @TokenRequired
    public ResponseEntity getAllUser() throws Exception{
        logger.debug("..............Inside Get AllUser..................");
        UserResponse userResponse = new UserResponse();
        ResponseEntity responseEntity = null;
        List<User> users = (List<User>) userService.getAll();
        logger.debug("User List Size :"+users.size());
        responseEntity =  new ResponseEntity(users,HttpStatus.OK);
        return responseEntity;
    }

}
