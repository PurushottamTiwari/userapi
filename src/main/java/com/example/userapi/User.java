package com.example.userapi;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name= "RESTAPIUSER",schema = "SYSTEM")
@SequenceGenerator(schema = "SYSTEM",name = "USER_SEQUENCE", sequenceName = "USER_SEQUENCE", allocationSize = 1, initialValue = 1)
public class User implements Serializable {
    @Id
    @Column(name = "USERID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "USER_SEQUENCE")
    int userId;

    @Column(name= "NAME")
    String name;

    @Column(name ="LASTUPDATE")
    Date lastUpdate;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
