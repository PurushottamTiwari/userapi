package com.example.userapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    public static final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    UserRepository userRepository;

    public void addUser(User  user){
        userRepository.save(user);
    }

    public void updateUser(User user){
        Optional<User> lcl_User = userRepository.findById(user.getUserId());
        User usr = lcl_User.get();
        usr.setName(user.getName());
        usr.setLastUpdate(new Date());
        logger.debug("AddUser :"+usr);
        userRepository.save(usr);
    }

    public User getUser(int userId){
        Optional<User> lcl_User = userRepository.findById(userId);
        User usr = lcl_User.get();
        logger.debug("GetUser :"+usr);
        return usr;
    }

    public Iterable<User> getAll(){
        return userRepository.findAll();
    }
}
