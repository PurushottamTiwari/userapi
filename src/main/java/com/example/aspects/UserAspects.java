package com.example.aspects;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.xml.bind.DatatypeConverter;

@Aspect
@Component
public class UserAspects {

    public static final Logger logger = LoggerFactory.getLogger(UserAspects.class);
    public  static final  String HEADER_PARAM_REF_TYPE = "SERVICE_IDENTIFIER_REFERENCE_TYPE";
    public  static final  String HEADER_PARAM_REF_VALUE = "SERVICE_IDENTIFIER_REFERENCE";



    @Before("@annotation(TokenRequired)")
    public void checkToken() throws Exception{
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        String token = ((ServletRequestAttributes)requestAttributes).getRequest().getHeader("Authorization");
        logger.debug("Token :"+token);

        if(StringUtils.isEmpty(token))throw new Exception("Invalid Request: Token Can't be empty");
        String key = ((ServletRequestAttributes)requestAttributes).getRequest().getHeader("Security-key");
        logger.debug("SecurityKey :"+key);

        if(StringUtils.isEmpty(key))throw new Exception("Invalid Request: Security-Key Can't be empty");
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(key)).parseClaimsJws(token);

        Claims claims  = claimsJws.getBody();
        if(claims == null)throw new Exception("Invalid Request");
        String subject = claims.getSubject();
        logger.debug("Subject :"+subject);
        String issuer = claims.getIssuer();
        logger.debug("Issuer :"+issuer);
        String header_param_ref_type = (String)claims.get(HEADER_PARAM_REF_TYPE);
        logger.debug("Parameter-Ref :"+header_param_ref_type);
        String header_param_ref_value = (String)claims.get(HEADER_PARAM_REF_VALUE);
        logger.debug("Ref-Value :"+header_param_ref_value);
        if(!"APIToken".equalsIgnoreCase(subject))throw new Exception("Parameter Mismatch :Subject");
        if(!"PurushottamTiwari".equalsIgnoreCase(issuer))throw new Exception("Parameter Mismatch :Issuer");
        if(!"EntityNumber".equalsIgnoreCase(header_param_ref_type))throw new Exception("Parameter Mismatch :SERVICE_IDENTIFIER_REFERENCE_TYPE");
        if(!"568452168".equalsIgnoreCase(header_param_ref_value))throw new Exception("Parameter Mismatch :SERVICE_IDENTIFIER_REFERENCE");

    }
}
