package com.example.exceptionhandling;

public class CustomErrorResponse {
    private String errorCode;
    private String errorString;
    private String errorParticular;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorParticular() {
        return errorParticular;
    }

    public void setErrorParticular(String errorParticular) {
        this.errorParticular = errorParticular;
    }

    @Override
    public String toString() {
        return "CustomErrorResponse{" +
                "errorCode='" + errorCode + '\'' +
                ", errorString='" + errorString + '\'' +
                ", errorParticular='" + errorParticular + '\'' +
                '}';
    }
}
