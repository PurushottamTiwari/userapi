package com.example.exceptionhandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
public class CustomExceptionHandler {

    public static final Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
    @ExceptionHandler(Exception.class)
    public ResponseEntity  apiExceptionHandler(Exception e){
        logger.error("Error Message :"+e.getMessage(), e);

        CustomErrorResponse customErrorResponse = new CustomErrorResponse();
        customErrorResponse.setErrorCode("400");
        customErrorResponse.setErrorString("Parameter Missing");
        customErrorResponse.setErrorParticular(e.getMessage());

        logger.error("CustomErrorResponse :"+customErrorResponse);
        ResponseEntity responseResponseEntity = new ResponseEntity(customErrorResponse, HttpStatus.BAD_REQUEST);
        return responseResponseEntity;
    }
}
